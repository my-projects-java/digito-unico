package br.com.bancointer.desafio.digitounico.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UsuarioSaidaModel {
    private Long id;
    private String nome;
    private String email;
}
