package br.com.bancointer.desafio.digitounico.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constantes {
    public static final String APPLICATION_JSON = "application/json";
    public static final String STATUS_CODE_200 = "Retorna um usuario";
    public static final String STATUS_CODE_400 = "Foi gerada uma exceção de negocio";
    public static final String STATUS_CODE_500 = "Foi gerada uma exceção";
}
