package br.com.bancointer.desafio.digitounico.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "digito_unico")
@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
public class DigitoUnicoEntity extends AbstractPK implements Serializable {

    private String digito;
    private Integer quantidadeConcatenacao;
    private Integer resultado;

    @JoinColumn(name = "idUsuario", referencedColumnName = "id",nullable = false,  updatable = false, foreignKey = @ForeignKey(name = "digito_unico_usuario"))
    @ManyToOne(fetch = FetchType.LAZY)
    private UsuarioEntity usuario;
}
