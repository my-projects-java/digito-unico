package br.com.bancointer.desafio.digitounico.domain.service;

import br.com.bancointer.desafio.digitounico.domain.entity.DigitoUnicoEntity;
import br.com.bancointer.desafio.digitounico.domain.repository.DigitoUnicoRepository;
import br.com.bancointer.desafio.digitounico.util.Cache;
import br.com.bancointer.desafio.digitounico.util.DigitoUnicoUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class DigitoUnicoService {
    private DigitoUnicoRepository repository;

    public List<DigitoUnicoEntity> obterCalculosPorIdUsuario(Long id) {
        return repository.findDigitoUnicoByIdUsuario(id);
    }

    public Integer calcular(DigitoUnicoEntity digitoUnicoEntrada) {
        Integer digitoUnico = DigitoUnicoUtil.calcular(digitoUnicoEntrada.getDigito(), digitoUnicoEntrada.getQuantidadeConcatenacao());

        if (Objects.nonNull(digitoUnicoEntrada.getUsuario())) {
            digitoUnicoEntrada.setResultado(digitoUnico);
            repository.save(digitoUnicoEntrada);
        }

        Cache.adicionar(digitoUnicoEntrada.getDigito(), digitoUnicoEntrada.getQuantidadeConcatenacao(), digitoUnico);
        return digitoUnico;
    }
}
