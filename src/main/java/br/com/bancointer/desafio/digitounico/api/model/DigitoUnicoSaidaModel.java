package br.com.bancointer.desafio.digitounico.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class DigitoUnicoSaidaModel implements Serializable {
    private String digito;
    private Integer quantidadeConcatenacao;
    private Integer resultado;
}
