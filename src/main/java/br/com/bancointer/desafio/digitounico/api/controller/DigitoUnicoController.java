package br.com.bancointer.desafio.digitounico.api.controller;

import br.com.bancointer.desafio.digitounico.api.model.DigitoUnicoEntradaModel;
import br.com.bancointer.desafio.digitounico.api.model.DigitoUnicoSaidaModel;
import br.com.bancointer.desafio.digitounico.api.model.UsuarioSaidaModel;
import br.com.bancointer.desafio.digitounico.domain.entity.DigitoUnicoEntity;
import br.com.bancointer.desafio.digitounico.domain.service.DigitoUnicoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static br.com.bancointer.desafio.digitounico.util.Constantes.*;


@RestController
@RequestMapping("v1/api/digitosunicos")
@AllArgsConstructor
public class DigitoUnicoController {
    private ModelMapper modelMapper;
    private DigitoUnicoService service;

    @GetMapping(path = "{id}", produces = APPLICATION_JSON)
    @ApiOperation(value = "Obtem calculo pela identificacao do usuario", response = UsuarioSaidaModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = STATUS_CODE_200),
            @ApiResponse(code = 400, message = STATUS_CODE_400),
            @ApiResponse(code = 500, message = STATUS_CODE_500)
    })
    public ResponseEntity<List<DigitoUnicoSaidaModel>> obterCalculosPorIdUsuario(@PathVariable Long id) {
        return ResponseEntity.ok(toCollectionModel(service.obterCalculosPorIdUsuario(id)));
    }

    @PostMapping
    @ApiOperation(value = "Calcula digito", response = UsuarioSaidaModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = STATUS_CODE_200),
            @ApiResponse(code = 400, message = STATUS_CODE_400),
            @ApiResponse(code = 500, message = STATUS_CODE_500)
    })
    public ResponseEntity<Integer> calcular(@RequestBody DigitoUnicoEntradaModel digitoUnicoEntrada) {
        return ResponseEntity.ok(service.calcular(toEntity(digitoUnicoEntrada)));
    }

    private DigitoUnicoSaidaModel toModel(DigitoUnicoEntity digitoUnicoEntity) {
        return modelMapper.map(digitoUnicoEntity, DigitoUnicoSaidaModel.class);
    }

    private DigitoUnicoEntity toEntity(DigitoUnicoEntradaModel digitoUnicoEntradaModel) {
        DigitoUnicoEntity digitoUnicoEntity = modelMapper.map(digitoUnicoEntradaModel, DigitoUnicoEntity.class);
        if(Objects.nonNull(digitoUnicoEntradaModel.getIdUsuario())){
            digitoUnicoEntity.getUsuario().setId(digitoUnicoEntradaModel.getIdUsuario());
        }
        return  digitoUnicoEntity;
    }

    private List<DigitoUnicoSaidaModel> toCollectionModel(List<DigitoUnicoEntity> digitoUnicoList) {
        return digitoUnicoList.stream().map(this::toModel).collect(Collectors.toList());
    }
}
