package br.com.bancointer.desafio.digitounico.api.exceptionhandler;

import br.com.bancointer.desafio.digitounico.domain.exception.NegocioException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;


@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NegocioException.class)
    public ResponseEntity<Object> processaNegocio(NegocioException ex, WebRequest request){
        var status = HttpStatus.BAD_REQUEST;
        var problema = Problema.builder()
                .status(status.value())
                .titulo("Um ou mais campos estão inválidos")
                .dataHora(OffsetDateTime.now())
                .mensagem(ex.getMessage())
                .build();
        return super.handleExceptionInternal(ex,problema,new HttpHeaders(),status, request);
    }

}
