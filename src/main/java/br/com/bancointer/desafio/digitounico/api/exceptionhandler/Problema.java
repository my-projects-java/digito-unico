package br.com.bancointer.desafio.digitounico.api.exceptionhandler;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
@Getter
@Setter
@Builder
public class Problema {
    private Integer status;
    private OffsetDateTime dataHora;
    private String titulo;
    private String mensagem;
}
