package br.com.bancointer.desafio.digitounico.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class UsuarioEntradaModel {
    @ApiModelProperty(value = "Identificacao do usuario")
    private Long id;

    @ApiModelProperty(value = "Nome do usuario")
    private String nome;

    @ApiModelProperty(value = "Email do usuario")
    private String email;

}
