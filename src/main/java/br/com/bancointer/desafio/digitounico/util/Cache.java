package br.com.bancointer.desafio.digitounico.util;

import lombok.experimental.UtilityClass;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@UtilityClass
public class Cache {
    private static LinkedHashMap<String, Integer> calculosRealizados = new LinkedHashMap<>();

    public static void adicionar(String digito, Integer quantidadeConcatenacao, Integer digitoUnico) {
        if (calculosRealizados.size() == 10) {
            removerChaveAntiga();
        }

        String chave = gerarNovaChave(digito, quantidadeConcatenacao);
        if (!procurarChave(chave)) {
            calculosRealizados.put(chave, digitoUnico);
        }
    }

    public static Integer buscar(String digito, Integer concatenacao) {
        Integer digitoUnico = null;
        String chave = gerarNovaChave(digito, concatenacao);
        if (procurarChave(chave)) {
            digitoUnico = calculosRealizados.get(chave);
        }
        return digitoUnico;
    }

    private static String gerarNovaChave(String digito, Integer quantidadeConcatenacao) {
        return digito.concat("-").concat(String.valueOf(quantidadeConcatenacao));
    }

    private static void removerChaveAntiga() {
        String chaveAntiga = calculosRealizados.keySet().stream().findFirst().orElse(null);
        calculosRealizados.remove(chaveAntiga);
    }

    private static boolean procurarChave(String chave) {
        return calculosRealizados.containsKey(chave);
    }

    public static Set<Map.Entry<String, Integer>> getCache() {
        return calculosRealizados.entrySet();
    }
}
