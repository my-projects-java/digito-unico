package br.com.bancointer.desafio.digitounico.util;

import lombok.experimental.UtilityClass;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UtilityClass
public class DigitoUnicoUtil {

    public static Integer calcular(String digito, Integer quantidadeConcatenacao) {
        if (quantidadeConcatenacao <= 0) {
            return somarDigitos(new BigInteger(digito));
        }

        String digitoConcatenado = IntStream.rangeClosed(1,quantidadeConcatenacao)
                .mapToObj(String::valueOf)
                .map(value -> digito)
                .collect(Collectors.joining());

        return somarDigitos(new BigInteger(digitoConcatenado));
    }

    private static Integer somarDigitos(BigInteger digitoConcatenado) {
        List<BigInteger> digitos = toList(digitoConcatenado);
        BigInteger resultado = digitos.stream().reduce(BigInteger.ZERO, BigInteger::add);
        if (BigInteger.valueOf(9).compareTo(resultado) < 0) {
            return somarDigitos(resultado);
        }
        return resultado.intValue();
    }

    private static List<BigInteger> toList(BigInteger digitoConcatenado) {
        return digitoConcatenado.toString()
                .chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList())
                .stream()
                .map(String::valueOf)
                .map(BigInteger::new)
                .collect(Collectors.toList());
    }
}