package br.com.bancointer.desafio.digitounico.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "usuario")
@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
public class UsuarioEntity extends AbstractPK implements Serializable {
    @NotBlank
    @Size(min = 1, max = 150, message = "Coluna NOME pode ter no máximo 150 caracteres")
    private String nome;

    @NotBlank
    @Size(min = 1, max = 500, message = "Coluna EMAIL pode ter no máximo 500 caracteres")
    private String email;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario", targetEntity = DigitoUnicoEntity.class)
    private Set<DigitoUnicoEntity> digitosUnicos = new HashSet<>();

}
