package br.com.bancointer.desafio.digitounico.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DigitoUnicoEntradaModel {
    @ApiModelProperty(value = "Identificacao do usuario")
    private Long idUsuario;

    @ApiModelProperty(value = "Digito")
    @JsonProperty(required = true)
    private String digito;

    @ApiModelProperty(value = "Quantidade de vezes a ser concatenado")
    @JsonProperty(required = true)
    private Integer quantidadeConcatenacao;
}
