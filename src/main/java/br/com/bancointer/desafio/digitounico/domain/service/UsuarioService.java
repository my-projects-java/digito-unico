package br.com.bancointer.desafio.digitounico.domain.service;

import br.com.bancointer.desafio.digitounico.domain.entity.UsuarioEntity;
import br.com.bancointer.desafio.digitounico.domain.exception.NegocioException;
import br.com.bancointer.desafio.digitounico.domain.repository.UsuarioRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UsuarioService {
    private UsuarioRepository repository;

    public UsuarioEntity obterPorId(Long id) {
        return repository.findById(id).orElseThrow(() -> new NegocioException("Usuario com id " + id + " não encontrado"));
    }

    public UsuarioEntity criar(UsuarioEntity usuario) {
        return repository.save(usuario);
    }

    public UsuarioEntity atualizar(UsuarioEntity usuarioEntity) {
        obterPorId(usuarioEntity.getId());
        repository.save(usuarioEntity);
        return usuarioEntity;
    }

    public void excluir(Long id) {
        obterPorId(id);
        repository.deleteById(id);
    }
}