package br.com.bancointer.desafio.digitounico.domain.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class AbstractPK {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

}
