package br.com.bancointer.desafio.digitounico.domain.repository;

import br.com.bancointer.desafio.digitounico.domain.entity.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<UsuarioEntity,Long> {
}
