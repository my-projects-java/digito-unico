package br.com.bancointer.desafio.digitounico.domain.repository;

import br.com.bancointer.desafio.digitounico.domain.entity.DigitoUnicoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnicoEntity,Long> {
    @Query(value = "SELECT * FROM DIGITO_UNICO  WHERE  ID_USUARIO = (:id_usuario)", nativeQuery = true)
    List<DigitoUnicoEntity> findDigitoUnicoByIdUsuario(@Param("id_usuario") Long id_usuario);
}
