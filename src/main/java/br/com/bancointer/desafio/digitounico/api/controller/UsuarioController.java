package br.com.bancointer.desafio.digitounico.api.controller;

import br.com.bancointer.desafio.digitounico.api.model.UsuarioEntradaModel;
import br.com.bancointer.desafio.digitounico.api.model.UsuarioSaidaModel;
import br.com.bancointer.desafio.digitounico.domain.entity.UsuarioEntity;
import br.com.bancointer.desafio.digitounico.domain.service.UsuarioService;
import br.com.bancointer.desafio.digitounico.util.Constantes;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static br.com.bancointer.desafio.digitounico.util.Constantes.*;

@RestController
@RequestMapping("v1/api/usuarios")
@AllArgsConstructor
public class UsuarioController {
    private ModelMapper modelMapper;
    private UsuarioService service;


    @GetMapping(path = "{id}", produces = APPLICATION_JSON)
    @ApiOperation(value = "Obtem usuario pela identificacao", response = UsuarioSaidaModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = STATUS_CODE_200),
            @ApiResponse(code = 400, message = STATUS_CODE_400),
            @ApiResponse(code = 500, message = STATUS_CODE_500)
    })
    public ResponseEntity<UsuarioSaidaModel> obterPorId(@PathVariable Long id){
        return ResponseEntity.ok(toModel(service.obterPorId(id)));
    }

    @PostMapping(produces = APPLICATION_JSON, consumes = APPLICATION_JSON)
    @ApiOperation(value = "Cadastrar um usuario", response = UsuarioSaidaModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = STATUS_CODE_200),
            @ApiResponse(code = 400, message = STATUS_CODE_400),
            @ApiResponse(code = 500, message = STATUS_CODE_500)
    })
    public ResponseEntity<UsuarioSaidaModel> criar(@RequestBody UsuarioEntradaModel usuarioEntradaModel){
        return ResponseEntity.status(HttpStatus.CREATED).body(toModel(service.criar(toEntity(usuarioEntradaModel))));
    }

    @PutMapping(produces = APPLICATION_JSON, consumes = APPLICATION_JSON)
    @ApiOperation(value = "Atualiza dados do usuario", response = UsuarioSaidaModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = STATUS_CODE_200),
            @ApiResponse(code = 400, message = STATUS_CODE_400),
            @ApiResponse(code = 500, message = STATUS_CODE_500)
    })
    public ResponseEntity<UsuarioSaidaModel> atualizar(@RequestBody UsuarioEntradaModel usuarioEntradaModel){
        return  ResponseEntity.ok(toModel(service.atualizar(toEntity(usuarioEntradaModel))));
    }

    @DeleteMapping(path = "{id}")
    @ApiOperation(value = "Exclui um usuario pela identificacao", response = UsuarioSaidaModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = STATUS_CODE_400),
            @ApiResponse(code = 500, message = STATUS_CODE_500)
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable Long id){
        service.excluir(id);
    }

//    TODO: Falta Implementar
    @PostMapping(path = "cadastrarchavepublica")
    public ResponseEntity<?> cadastrarChavePublica(@PathVariable String chavePublica){
        return ResponseEntity.ok(null);
    }

    private UsuarioSaidaModel toModel(UsuarioEntity usuarioEntity){
        return modelMapper.map(usuarioEntity, UsuarioSaidaModel.class);
    }
    private  UsuarioEntity toEntity(UsuarioEntradaModel usuarioEntradaModel){
        return modelMapper.map(usuarioEntradaModel,UsuarioEntity.class);
    }
}
