# Desafio Digito Único Inter
Projeto consistem em gerar digito unico associado opcionalmente a um usuario, seguindos as regras:

Dado dois números n e k, P deverá ser criado da concatenação da string n * k.

###### Exemplo:
> n=9875 e k=4 então p = 9875 9875 9875 9875

> digitoUnico = digitoUnico(9875987598759875)digitoUnico = digitoUnico(9875987598759875)

> 5+7+8+9+5+7+8+9+5+7+8+9+5+7+8+9=1165+7+8+9+5+7+8+9+5+7+8+9+5+7+8+9=116

> digitoUnico = digitoUnico(116)digitoUnico = digitoUnico(116)

> 1+1+6=81+1+6=8

> digitoUnico = digitoUnico(8)
## APIs
	1. Deverá ser disponibilizado endpoints para o CRUD de usuários.- [Feito] 
	2. Deverá ser disponibilizado um endpoint para cálculo do dígito.- [Feito] 
       este cálculo pode ser associado de forma opcional a um usuário.
	3. Deverá ser criado um endpoint que recupera todos os cálculos
       para um determinado usuário.- [Feito] 
	4. Deverá ser criado um endpoint para enviar a chave pública do
       usuário que será utilizada para a criptografia. Esta API deverá
	   receber uma string que conterá a chave.- [A Fazer] 

## Ferramentas Utilizadas
1. Java 11
2. SpringBoot 2.4.0
3. Banco em memoria H2
4. Swagger
5. Lombok
6. IntelliJ CE
7. Postman
8. Git/GitLab

## Execução
#### Pelo Codigo fonte
	Executar a classe br.com.bancointer.desafio.digitounico.DigitoUnicoApplication
#### Pelo Artefato
	java -jar digito-unico-inter.jar

## Acessar o contrato (Documentação da API)
	http://localhost:8080/swagger-ui/index.html

## Acessar testes integrados com Postman
	Na raiz do codigo fonte encontra-se o arquivo postman_collection.json

## Contatos
    Nome: Éden Alencar
    E-mail: eden.alencar@gmail.com
    Celular: 31 9 9148-6150

																					
_Feito com 🧡 em Belo Horizonte!_